#ifndef LOL_STRING_H
#define LOL_STRING_H

#include <stdlib.h>
#include <string.h>
#include <exception>
#include <iostream>

class LolStringException: public std::exception {};
class StrMemException: public LolStringException {};

namespace lol {

class String {
	enum { FACTOR = 2 };
public:
	friend std::ostream &operator <<(std::ostream &stream, const String &str);

	String(const char *str) {
		size = strlen(str) + 1;
		buffer = static_cast<char*>(malloc(size));
		if ( buffer == NULL ) {
			throw StrMemException();
		}
		strcpy(buffer, str);
	}
	
	String(const String &other) {
		size = strlen(other.buffer) + 1;
		buffer = static_cast<char*>(malloc(size));
		if ( buffer == NULL ) {
			throw StrMemException();
		}
		strcpy(buffer, other.buffer);
	}
	
	~String() {
		free(buffer);
	}
	
	const String &operator =(const String &other) {
		size_t newSize = strlen(other.buffer) + 1;
		if ( newSize > size ) {
			free(buffer);
			buffer = static_cast<char*>(malloc(newSize));
			if ( buffer == NULL ) {
				throw StrMemException();
			}
			size = newSize;
		}
		strcpy(buffer, other.buffer);

        return *this;
	}
	
	const char *c_str() const {
		return buffer;
	}
private:
	char *buffer;
	size_t size;
};

std::ostream &operator <<(std::ostream &stream, const String &str) {
	return stream << str.c_str();
}

} //namespace lol

#endif // LOL_STRING_H
