#ifndef LOL_ITERATOR_TRAITS_H
#define LOL_ITERATOR_TRAITS_H

#include <iterator>

namespace lol {

template <class Iterator> struct IteratorTraits {
	typedef typename Iterator::reference reference;
	typedef typename Iterator::pointer pointer;
	typedef typename Iterator::difference_type difference_type;
	typedef typename Iterator::value_type value_type;
	typedef typename Iterator::iterator_category iterator_category;
};

template <class T> struct IteratorTraits<T*> {
	typedef T& reference;
	typedef T* pointer;
	typedef size_t difference_type;
	typedef T value_type;
	typedef std::random_access_iterator_tag iterator_category;
};

template <class Iterator> typename IteratorTraits<Iterator>::iterator_category iteratorCategory() {
    return typename IteratorTraits<Iterator>::iterator_category();
}

} // namespace lol

#endif // LOL_ITERATOR_TRAITS_H
