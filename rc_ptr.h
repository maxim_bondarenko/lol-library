#ifndef rc_ptr_h
#define rc_ptr_h


template <class T>
class rc_ptr
{
public:
    rc_ptr( T *p = 0 )
    {
        pointee_ = p;
        counter_ = new int( 1 );
    }

    rc_ptr( rc_ptr< T >& rhs )
    : pointee_( rhs.get() ), counter_( rhs.counter() )
    {
        ++( *counter_ );
    }

    template < class U >
    rc_ptr( rc_ptr< U >& rhs )
    : pointee_( rhs.get() ), counter_( rhs.counter() )
    {
        ++( *counter_ );
    }

    ~rc_ptr()
    {
        DecRefCount();
    }

    template < class U >
    rc_ptr< T >& operator=( rc_ptr<U> &rhs )
    {
        if ( &rhs == this )
        {
            return *this;
        }
        DecRefCount();
        pointee_ = rhs.get();
        counter_ = rhs.counter();
        ++*counter_;

        return *this;
    }

    T& operator*() const
    {
        return *pointee_;
    }

    T* operator->() const
    {
        return pointee_;
    }

    T* get() const
    {
        return pointee_;
    }

    int *counter() const
    {
        return counter_;
    }

    void reset( T *p = 0 )
    {
        --( *counter_ );
        if ( *counter_ == 0 )
        {
            delete pointee_;
        }

        pointee_ = p;
        counter_ = new int( 1 );
    }

    int* counter()
    {
        return counter_;
    }
private:
    T* pointee_;
    int* counter_;

    void DecRefCount()
    {
        --( *counter_ );
        if ( *counter_ == 0 )
        {
            delete pointee_;
            delete counter_;
        }
    }
};


#endif // rc_ptr_h