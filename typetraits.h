#ifndef LOL_TYPE_TRAITS_H
#define LOL_TYPE_TRAITS_H

#include "rc_ptr.h"

namespace lol {

template <class T>
struct TypeTraits {
	enum { isAtomicType = false };
};

template <>
struct TypeTraits<int> {
	enum { isAtomicType = true };
};

template <>
struct TypeTraits<double> {
	enum { isAtomicType = true };
};

template <class T>
struct TypeTraits<T*> {
    enum { isAtomicType = true };
};

template <class T>
struct TypeTraits<rc_ptr<T> > {
    enum { isAtomicType = true };
};

}

#endif // LOL_TYPE_TRAITS_H
