#ifndef LOL_VECTOR_H
#define LOL_VECTOR_H

#include <stdlib.h>
#include <exception>
#include <iostream>
#include <vector>
#include <iterator>
#include "typetraits.h"
#include "IteratorTraits.h"

using namespace std;

namespace lol {

class VectorException: public std::exception {}; 
class VecMemException: public VectorException {};
class OutOfRange: public VectorException {};
class VectorIsEmpty: public VectorException {};

template <class T, bool isAtomicType = TypeTraits<T>::isAtomicType>
class Vector;

template <class T, class PtrType = T*, class RefType = T&>
class vector_reverse_iterator {
	public:
		typedef RefType reference;
		typedef PtrType pointer;
		typedef size_t difference_type;
		typedef T value_type;
		typedef std::random_access_iterator_tag iterator_category;
	
		vector_reverse_iterator(PtrType p) : ptr(p) {}
		
		const vector_reverse_iterator &operator ++() {
			--ptr;
			
			return *this;
		}
		
		vector_reverse_iterator operator ++(int) {
			PtrType tmp = ptr;
			--ptr;
			
			return tmp;
		}
		
		const vector_reverse_iterator &operator --() {
			++ptr;
			
			return *this;
		}
		
		vector_reverse_iterator operator --(int) {
			PtrType tmp = ptr;
			++ptr;
			
			return tmp;
		}
		
		const vector_reverse_iterator &operator +=(size_t value) {
			ptr -= value;
		}
		
		const vector_reverse_iterator &operator -=(size_t value) {
			ptr += value;
		}
		
		vector_reverse_iterator operator +(size_t value) {
			vector_reverse_iterator tmp(*this);
			tmp += value;
			
			return tmp;
		}
		
		vector_reverse_iterator operator -(size_t value) {
			vector_reverse_iterator tmp(*this);
			tmp -= value;
			
			return tmp;
		}
		
		size_t operator -(const vector_reverse_iterator &other) {
			return other.ptr - ptr;
		}
		
		RefType operator *() {
			return *ptr;
		}
		
		RefType operator ->() {
			return ptr;
		}
		
		bool operator ==(const vector_reverse_iterator& other) {
			return ptr == other.ptr;
		}
		
		bool operator !=(const vector_reverse_iterator& other) {
			return ptr != other.ptr;
		}
	private:
		PtrType ptr;
};

template <class T>
class Vector< T, true >  {
	enum {MIN_CAPACITY = 10, FACTOR = 13, DENOMINATOR = 10};

public:	
	typedef T* iterator;
	typedef vector_reverse_iterator<T> reverse_iterator;
	typedef const T* const_iterator;
	typedef vector_reverse_iterator<T, const T*, const T&> const_reverse_iterator;

	typedef T& reference;
	typedef const T& const_reference;
	typedef T* pointer;
	typedef const T* const_pointer;
	
public:
    Vector() : buffer_(NULL), size_(0), capacity_(0) {
	}
	
	Vector(size_t size, const T &value = T()) : size_(0), buffer_(NULL), capacity_(0) {
		resize(size, value);
	}
	
    template <class Iterator> Vector(Iterator begin, Iterator end) : buffer_(NULL), size_(0), capacity_(0) {
		construct(begin, end, iteratorCategory<Iterator>());
    }

	template <class U> Vector(const Vector<U> &other) : size_(0), capacity_(0), buffer_(NULL) {
		*this = other;
	}
		
	~Vector() {
        free(buffer_);
    }
	
	template <class U> const Vector<T> &operator =(const Vector<U> &other) {
		assign(other.begin(), other.end());
		
		return *this;
	}
	
	void reserve(size_t newCapacity) {
		if ( newCapacity < capacity_ ) {
			return;
		}
		capacity_ = newCapacity;

        buffer_ = static_cast<T*>(realloc(buffer_, capacity_*sizeof(T)));
		if ( buffer_ == NULL ) {
			throw VecMemException();
        }
	}
	
	void resize(size_t newSize, const T& value = T()) {
        buffer_ = static_cast<T*>(realloc(buffer_, newSize*sizeof(T)));
		if ( buffer_ == NULL ) {
			throw VecMemException();
        }

        size_ = newSize;
		capacity_ = size_;
	}
	
    void push_back(const T &value) {
        if ( size_ == capacity_ ) {
            if ( capacity_ < MIN_CAPACITY ) {
                reserve(MIN_CAPACITY);
            } else {
                reserve(capacity_ * FACTOR / DENOMINATOR);
            }
        }
        *(buffer_ + size_) = value;
        size_ += 1;
    }

    void pop_back() {
        if ( size_ == 0 ) {
            throw VectorIsEmpty();
        }
        size_ -= 1;
    }

    reference operator [](size_t index) {
		return *(buffer_ + index);
	}
	
	const_reference operator [](size_t index) const {
		return *(buffer_ + index);
	}
	
	reference at(size_t index) {
		if ( index >= size_ ) {
			throw OutOfRange();
		}

		return *(buffer_ + index);
	}
	
	const_reference at(size_t index) const {
		if ( index >= size_ ) {
			throw OutOfRange();
		}
		
		return *(buffer_ + index);
	}
	
	template <class Iterator>
	void assign(Iterator begin, Iterator end) {
		assignToRange(begin, end, iteratorCategory<Iterator>());
	}
	
	void assign(size_t newSize, const T &value) {
		resize(newSize);
		for ( iterator to = buffer_, end = buffer_ + size_; to != end; ++to ) {
			*to = value;
		}
	}
	
	iterator insert(iterator position, const T &value) {
		size_t offset = position - buffer_;
		insert(position, 1, value);
		
		return buffer_ + offset;
	}
	
	void insert(iterator position, size_t quantity, const T& value) {
        size_t offset = position - buffer_;

        reserve(size_ + quantity);
        size_ += quantity;
        position = buffer_ + offset;
        for ( iterator to = buffer_ + size_ - 1, from = to - quantity; from >= position; --to, --from) {
			*to = *from;
		}
		for ( ; quantity > 0; --quantity, ++position ) {
			*position = value;
		}
	}	
	
	template <class Iterator>
	void insert(iterator position, Iterator begin, Iterator end) {
		insertRange(position, begin, end, iteratorCategory<Iterator>());
	}

    iterator erase(iterator position) {
        for ( iterator to = position, from = position + 1, vecEnd = end(); from != vecEnd; ++to, ++from ) {
            *to = *from;
        }
        size_ -= 1;

        return position;
    }

    iterator erase(iterator first, iterator last) {
        size_ -= last - first;
        for ( iterator to = first, vecEnd = end(); last != vecEnd; ++to, ++last ) {
            *to = *last;
        }

        return first;
    }

    void swap(Vector<T> &other) {
        std::swap(buffer_, other.buffer_);
        std::swap(size_, other.size_);
        std::swap(capacity_, other.capacity_);
    }
	
	bool empty() {
		return size_ == 0;
	}
	
	size_t size() {
		return size_;
	}
	
	size_t capacity() {
		return capacity_;
	}
	
	void clear() {
		resize(0);
	}
	
	iterator begin() {
		return buffer_;
	}
	
	const_iterator begin() const {
		return buffer_;
	}
	
	iterator end() {
		return buffer_ + size_;
	}
	
	const_iterator end() const {
		return buffer_ + size_;
	}
	
	reverse_iterator rbegin() {
		return reverse_iterator(buffer_ + size_ - 1);
	}
	
	const_reverse_iterator rbegin() const {
		return const_reverse_iterator(buffer_ + size_ - 1);
	}
	
	reverse_iterator rend() {
		return reverse_iterator(buffer_ - 1);
	}
	
	const_reverse_iterator rend() const {
		return const_reverse_iterator(buffer_ - 1);
	}
private:
    template <class Iterator>
    void construct(Iterator begin, Iterator end, std::random_access_iterator_tag) {
        resize(end - begin);

        for ( iterator to = buffer_; begin != end; ++to, ++begin ) {
			*to = *begin;
		}
	}
	
    template <class Iterator, class IteratorCategory>
    void construct(Iterator begin, Iterator end, IteratorCategory) {
		for ( ; begin != end; ++begin ) {
			push_back(*begin);
		}
	}	
	
    template <class Iterator>
    void assignToRange(Iterator begin, Iterator end, std::random_access_iterator_tag) {
		resize(end - begin);

        for ( iterator to = buffer_; begin != end; ++to, ++begin ) {
			*to = *begin;
		}
	}
	
    template <class Iterator, class IteratorCategory>
    void assignToRange(Iterator begin, Iterator end, IteratorCategory) {
		size_ = 0;

        for ( ; begin != end; ++begin ) {
			push_back(*begin);
		}
		resize(size_);
	}
	
	template <class Iterator> 
	void insertRange(iterator position, 
					Iterator begin, 
					Iterator end, 
					std::random_access_iterator_tag) {
		size_t offset = position - buffer_;
		size_t quantity = end - begin;
		size_t newSize = size_ + quantity;
		resize(newSize);
		position = buffer_ + offset;
		
		for ( iterator to = buffer_ + newSize - 1, from = to - quantity; from >= position; --to, --from ) {
			*to = *from;
		}
		for ( ; begin != end; ++begin, ++position ) {
			*position = *begin;
		}
	}
	
    template <class Iterator, class IteratorCategory>
    void insertRange(iterator position, Iterator begin, Iterator end, IteratorCategory) {
        Vector<T> tmp;
		size_t offset = position - buffer_;
		
		iterator to = position;
		iterator dataEnd = buffer_ + size_;
		iterator bufEnd = buffer_ + capacity_;
		
		for ( ; to != dataEnd && begin != end; ++begin, ++to ) {
			tmp.push_back(*to);
			*to = *begin;	
		}
		for ( ; to != bufEnd && begin != end; ++to, ++begin, ++size_ ) {
			*to = *begin;
		}
		
		size_t quantity = to - position;
		size_t dataEndOffset = offset + quantity;
		if ( dataEndOffset > size_ ) {
			size_ = dataEndOffset;
		}
		
		for ( ; begin != end; ++begin, ++quantity ) {
			push_back(*begin);
		}
		
        resize(size_ + tmp.size());
		iterator oldDataBegin = buffer_ + offset + quantity;

		for ( iterator to = buffer_ + size_ - 1, from = to - quantity; from >= oldDataBegin; --to, --from ) {
			*to = *from;
		}
		
		iterator from = tmp.begin();
		iterator tmpEnd = tmp.end();

		for ( iterator to = oldDataBegin; from != tmpEnd; ++to, ++from ) {
			*to = *from;
		}
	}
private:
	T* buffer_;
	size_t size_;
	size_t capacity_;
};

template <class T>
class Vector<T, false> {
public:
	typedef typename vector<T>::iterator iterator;
	typedef typename vector<T>::const_iterator const_iterator;

	Vector() : impl() {}
	Vector(size_t newCapacity) {
		impl.reserve(newCapacity);
	}
	template <class Iterator> Vector(Iterator begin, Iterator end) : impl(begin, end) {}
	
	T& operator [] (size_t index) {
		return impl[index];
	}
	
	void push_back(const T &value) {
		// cout << "In std implementation Vector::push_back()\n";
		impl.push_back(value);
	}
	
	size_t size() {
		return impl.size();
	}
	
	iterator begin() {
		return impl.begin();
	}
	
	iterator end() {
		return impl.end();
	}
private:
	std::vector<T> impl;
};

}

#endif // LOL_VECTOR_H
