#include <iostream>
#include "lolString.h"
#include "Vector.h"
#include <string>
#include <vector>
#include <algorithm>
#include "rl_ptr.h"
#include "rc_ptr.h"
#include <list>

using namespace lol;
using namespace std;

class A {
public:
	A(const String &name) : name(name) { cout << "in A constructor\n"; }
	
	void appendStr(const String &str) {
		strings.push_back(str);
	}
	
private:
	String name;
	Vector<String> strings;
};

template <class T> std::ostream &operator <<(std::ostream &out, std::vector<T> v) {
	std::ostream_iterator<T> outIt(out, "\n");
	std::copy(v.begin(), v.end(), outIt);
	
	return out;
}

int main() {
	int array[] = {1, 2, 3, 4};
	
	std::list<int> l1(array, array+4);
	
	Vector<int> ints(l1.begin(), l1.end());
	ints.push_back(42);
	ints.push_back(21);
	
	cout << ints << endl;
	
	ints.insert(ints.begin() + 4, l1.begin(), l1.end());
	cout << ints << endl;
	
	ints.insert(ints.begin() + 2, l1.begin(), l1.end());
	cout << ints << endl;
	
	ints.insert(ints.begin() + 5, (size_t)10, 44);
	ints.insert(ints.begin() + 2, (size_t)10, 33);
	cout << ints << endl;
	
	cout << "cout complete, size is " << ints.size() << ", capacity is " << ints.capacity() << " \n";
	
	vector<int> newInts(ints.begin(), ints.end());
	cout << newInts << endl;
	
	ints.insert(ints.begin() + 10, newInts.begin(), newInts.end());
	cout << ints << endl;
	
	Vector<int> ints2(newInts.rbegin(), newInts.rend());
	
	Vector<string, true> strings;
	strings.push_back("hello!");
	strings[0] = "Hello stl and lol!";
	cout << strings[0] << endl;
	
	Vector<double> doubles;
	doubles.push_back(42.42);
	cout << doubles[0] << endl;
	
	Vector<string*, true> pStrings;
	pStrings.push_back(new string("hello pointers!"));
	cout << *pStrings[0] << endl;
	
	Vector< rc_ptr<string>, true > kokiStrings;
	rc_ptr<string> str(new string("hello koki!"));
	kokiStrings.push_back(str);
	cout << *kokiStrings[0] << endl;
	
	Vector<String> lolStrings;
	lolStrings.push_back("Hello lol!");
	cout << lolStrings[0] << endl;
	
	return 0;
}